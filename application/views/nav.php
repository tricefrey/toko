<!DOCTYPE html>
<html>
	<title>
		<?php echo $title;?>
	</title>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="/p1/main">Web Name</a>
				</div>
				<ul class="nav navbar-nav">
					<li class=<?php echo $nav1; ?>><a href="/p1/main">Main</a></li>
					<li class=<?php echo $nav2; ?>><a href="/p1/main/table">Table</a></li>
					<li class=<?php echo $nav3; ?>><a href="/p1/main/faq">FAQ</a></li>
					<li class=<?php echo $nav4; ?>><a href="#">Menu 3</a></li>        
				</ul>
				<a href="#" class="btn btn-info navbar-btn pull-right" role="button" data-toggle="modal" data-target="#logout">Log Out</a>
			</div>
		</nav>
		<div class="modal fade" id="logout" role="dialog">
    		<div class="modal-dialog modal-sm">
      			<div class="modal-content">
        			<div class="modal-header">
          				<button type="button" class="close" data-dismiss="modal">&times;</button>
          				<h4 class="modal-title" align="center">Log Out</h4>
        			</div>
        			<div class="modal-body">
          				<p align="center">Do you want to log out ?</p></br>
          				<p style="padding:0px 0px 10px 75px"><a href="/p1/main/login"class="btn btn-info" role="button">Yes</a>
          				&nbsp;&nbsp;&nbsp;&nbsp;
          				<button type="button" class="btn btn-info" data-dismiss="modal" left=30px>No</button>
          				</p>
          			</div>
      			</div>
    		</div>
  		</div>
  		
	</body>	
</html>