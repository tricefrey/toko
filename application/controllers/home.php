<?php
	class home extends  CI_Controller
	{
		function construct()
		{
			parent::__construct();
			$this->load->model('model_user');
		}	
		public function index()
		{
			$this->home();
		}
		public function home()
		{
			$data['title'] = 'Home title';
			$data['header'] = 'page header';
			$data['name'] = $this->model_user->getname();
			
			$this->load->view('view_home', $data);
		}
		public function name()
		{
			$this->load->model('model_user');
		
			$data['ntitle'] = 'users name';
			$data['nheader'] = 'nama para user';
			$data['name'] = $this->model_user->getname();
		
			$this->load->view('name',$data);
		}
	}
	
?>