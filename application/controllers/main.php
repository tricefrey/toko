<?php
class main extends CI_Controller 
{
	public function index()
	{
		$this->home();
	}
	public function home()
	{
		$data['title'] = 'Halaman Utama';
		$data['header'] = 'Main Page';
		$data['nav1'] = '"active"';
		$data['nav2'] = '"#"';
		$data['nav3'] = '"#"';
		$data['nav4'] = '"#"';
		$this->load->view('nav',$data);	
		$this->load->view('head',$data);
		$this->load->view('utama',$data);
		$this->load->view('footer',$data);
	}
	public function table()
	{
		$this->load->model('model_user');
		
		$data['nav1'] = '"#"';
		$data['nav2'] = '"active"';
		$data['nav3'] = '"#"';
		$data['nav4'] = '"#"';
		$data['title'] = 'daftar user';
		$data['header'] = 'User List';
		$data['user'] = $this->model_user->getuser();
		$this->load->view('nav',$data);
		$this->load->view('head',$data);	
		$this->load->view('table',$data);
		$this->load->view('footer',$data);
		
	}
	public function login()
	{
		$data['title'] = 'Log-in';
		$this->load->view('login',$data);
	}
	public function faq()
	{
		$data['title'] = 'FAQ';
		$data['header'] = 'Frequently Asked Question';
		$data['nav1'] = '"#"';
		$data['nav2'] = '"#"';
		$data['nav3'] = '"faq"';
		$data['nav4'] = '"#"';
		$this->load->view('nav',$data);	
		$this->load->view('head',$data);
		$this->load->view('faq',$data);
		$this->load->view('footer',$data);
	}
}
?>